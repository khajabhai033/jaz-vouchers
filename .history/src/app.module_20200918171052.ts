import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { VouchersOpModule } from './vouchers-op/vouchers-op.module';

@Module({
  imports: [VouchersOpModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
