import { Module } from '@nestjs/common';
import { GraphQLFederationModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VouchersOpModule } from './vouchers-op/vouchers-op.module';
import { typeOrmConfig } from './config/typeOrm.config';
import * as config from 'config';

const graphqlConf = config.get('graphql');

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    GraphQLFederationModule.forRoot({
      autoSchemaFile: true,
      playground: process.env.playground || graphqlConf.playground,
      path: '/api/vouchers',
    }),
    VouchersOpModule,
  ],
})
export class AppModule {}
