import { Field, InputType, Int } from '@nestjs/graphql';

@InputType()
export class Vouchers_opInput {
  @Field({ nullable: true })
  code: string;

  @Field({ nullable: true })
  type: string;

  @Field({ nullable: true })
  name: string;

  @Field({ nullable: true })
  name_ar: string;

  @Field({ nullable: true })
  is_multiple: boolean;

  @Field({ nullable: true })
  is_active: boolean;

  @Field({ nullable: true })
  auto_apply: boolean;

  @Field({ nullable: true })
  status: string;

  @Field({ nullable: true })
  relation_type: string;

  @Field(type => [Int], { nullable: true })
  relation: any;

  @Field({ nullable: true })
  voucher_type: string;

  @Field({ nullable: true })
  percentage: number;

  @Field({ nullable: true })
  min_order_amount: number;

  @Field({ nullable: true })
  max_order_amount: number;

  @Field({ nullable: true })
  amount_used_till_date: number;

  @Field({ nullable: true })
  min_discount_amount: number;

  @Field({ nullable: true })
  max_discount_amount: number;

  @Field({ nullable: true })
  min_quantity: number;

  @Field({ nullable: true })
  max_quantity: number;

  @Field({ nullable: true })
  max_quota: number;

  @Field({ nullable: true })
  country: number;

  @Field({ nullable: true })
  city: number;

  @Field({ nullable: true })
  currency_type: string;

  @Field({ nullable: true })
  communication_channel: string;

  @Field({ nullable: true })
  offer_ratio: string;

  @Field({ nullable: true })
  dataareaid: number;

  @Field({ nullable: true })
  terms: string;

  @Field({ nullable: true })
  terms_ar: string;

  @Field({ nullable: true })
  start_date: Date;

  @Field({ nullable: true })
  end_date: Date;

  @Field({ nullable: true })
  created_by: string;

  @Field({ nullable: true })
  updated_by: string;

  @Field({ nullable: true })
  created_at: Date;

  @Field({ nullable: true })
  updated_at: Date;

  @Field(type => [Int], { nullable: true })
  users: any;

  @Field({ nullable: true })
  user_type: string;

  @Field({ nullable: true })
  on_order: boolean;
}
