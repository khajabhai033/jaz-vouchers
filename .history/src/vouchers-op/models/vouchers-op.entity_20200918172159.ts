import { ObjectType } from "@nestjs/graphql"
import { Entity } from "typeorm"

@ObjectType()
@Entity()
export class Vouchers_op {

    id:bigint                      |           | not null | nextval('vouchers_op_id_seq'::regclass)
    code                  | character varying           |           |          | 
    type                  | character varying           |           |          | 
    name                  | character varying           |           |          | 
    name_ar               | character varying           |           |          | 
    is_multiple           | boolean                     |           |          | 
    is_active             | boolean                     |           |          | 
    auto_apply            | boolean                     |           |          | 
    status                | character varying           |           |          | 
    relation_type         | character varying           |           |          | 
    relation              | bigint[]                    |           |          | ARRAY[]::bigint[]
    voucher_type          | character varying           |           |          | 
    percentage            | integer                     |           |          | 
    min_order_amount      | integer                     |           |          | 
    max_order_amount      | integer                     |           |          | 
    amount_used_till_date | integer                     |           |          | 
    min_discount_amount   | integer                     |           |          | 
    max_discount_amount   | integer                     |           |          | 
    min_quantity          | integer                     |           |          | 
    max_quantity          | integer                     |           |          | 
    max_quota             | integer                     |           |          | 
    country               | integer                     |           |          | 
    city                  | integer                     |           |          | 
    currency_type         | character varying           |           |          | 
    communication_channel | character varying           |           |          | 
    offer_ratio           | character varying           |           |          | 
    dataareaid            | integer                     |           |          | 
    terms                 | text                        |           |          | 
    terms_ar              | text                        |           |          | 
    start_date            | timestamp without time zone |           |          | 
    end_date              | timestamp without time zone |           |          | 
    created_by            | character varying           |           |          | 
    updated_by            | character varying           |           |          | 
    created_at            | timestamp without time zone |           |          | 
    updated_at            | timestamp without time zone |           |          | 
    users                 | bigint[]                    |           |          | ARRAY[]::bigint[]
    user_type             | character varying           |           |          | 
    on_order              | boolean                     |           |          | false
   
}
