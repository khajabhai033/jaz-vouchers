import { Field, ObjectType } from '@nestjs/graphql';
import { Entity } from 'typeorm';

@ObjectType()
@Entity()
export class Vouchers_op {
  @Field()
  id: bigint;
  @Field()
  code: string;
  type: string;
  name: string;
  name_ar: string;
  is_multiple: boolean;
  is_active: boolean;
  auto_apply: boolean;
  status: string;
  relation_type: string;
  relation: bigint[];
  voucher_type: string;
  percentage: number;
  min_order_amount: number;
  max_order_amount: number;
  amount_used_till_date: number;
  min_discount_amount: number;
  max_discount_amount: number;
  min_quantity: number;
  max_quantity: number;
  max_quota: number;
  country: number;
  city: number;
  currency_type: string;
  communication_channel: string;
  offer_ratio: string;
  dataareaid: number;
  terms: string;
  terms_ar: string;
  start_date: Date;
  end_date: Date;
  created_by: string;
  updated_by: string;
  created_at: Date;
  updated_at: Date;
  users: bigint[];
  user_type: string;
  on_order: boolean;
}
