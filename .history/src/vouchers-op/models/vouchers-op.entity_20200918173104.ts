import { Field, ObjectType } from '@nestjs/graphql';
import { Entity } from 'typeorm';

@ObjectType()
@Entity('vouchers_op')
export class Vouchers_op {
  @Field()
  id: bigint;

  @Field()
  code: string;

  @Field()
  type: string;

  @Field()
  name: string;

  @Field()
  name_ar: string;

  @Field()
  is_multiple: boolean;

  @Field()
  is_active: boolean;

  @Field()
  auto_apply: boolean;

  @Field()
  status: string;

  @Field()
  relation_type: string;

  @Field()
  relation: bigint[];

  @Field()
  voucher_type: string;

  @Field()
  percentage: number;

  @Field()
  min_order_amount: number;

  @Field()
  max_order_amount: number;

  @Field()
  amount_used_till_date: number;

  @Field()
  min_discount_amount: number;

  @Field()
  max_discount_amount: number;

  @Field()
  min_quantity: number;

  @Field()
  max_quantity: number;

  @Field()
  max_quota: number;

  @Field()
  country: number;

  @Field()
  city: number;

  @Field()
  currency_type: string;

  @Field()
  communication_channel: string;

  @Field()
  offer_ratio: string;

  @Field()
  dataareaid: number;

  @Field()
  terms: string;

  @Field()
  terms_ar: string;

  @Field()
  start_date: Date;

  @Field()
  end_date: Date;

  @Field()
  created_by: string;

  @Field()
  updated_by: string;

  @Field()
  created_at: Date;

  @Field()
  updated_at: Date;

  @Field()
  users: bigint[];

  @Field()
  user_type: string;

  @Field()
  on_order: boolean;
}
