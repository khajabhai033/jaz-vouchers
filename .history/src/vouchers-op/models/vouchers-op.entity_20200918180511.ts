import {
  Field,
  GraphQLISODateTime,
  ID,
  ObjectType,
  Int,
} from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn, Index } from 'typeorm';

@ObjectType()
@Entity('vouchers_op')
export class Vouchers_op {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  //   @Index('vouchers_op_pkey')
  id: bigint;

  @Field()
  @Column()
  code: string;

  @Field()
  @Column()
  type: string;

  @Field()
  @Column()
  name: string;

  @Field()
  @Column()
  name_ar: string;

  @Field()
  @Column()
  is_multiple: boolean;

  @Field()
  @Column()
  is_active: boolean;

  @Field()
  @Column()
  auto_apply: boolean;

  @Field()
  @Column()
  status: string;

  @Field()
  @Column()
  relation_type: string;

  @Field(type => [Int])
  @Column('bigint', { array: true })
  relation: any;

  @Field()
  @Column()
  voucher_type: string;

  @Field()
  @Column()
  percentage: number;

  @Field()
  @Column()
  min_order_amount: number;

  @Field()
  @Column()
  max_order_amount: number;

  @Field()
  @Column()
  amount_used_till_date: number;

  @Field()
  @Column()
  min_discount_amount: number;

  @Field()
  @Column()
  max_discount_amount: number;

  @Field()
  @Column()
  min_quantity: number;

  @Field()
  @Column()
  max_quantity: number;

  @Field()
  @Column()
  max_quota: number;

  @Field()
  @Column()
  country: number;

  @Field()
  @Column()
  city: number;

  @Field()
  @Column()
  currency_type: string;

  @Field()
  @Column()
  communication_channel: string;

  @Field()
  @Column()
  offer_ratio: string;

  @Field()
  @Column()
  dataareaid: number;

  @Field()
  @Column()
  terms: string;

  @Field()
  @Column()
  terms_ar: string;

  @Field(type => GraphQLISODateTime)
  @Column()
  start_date: Date;

  @Field(type => GraphQLISODateTime)
  @Column()
  end_date: Date;

  @Field()
  @Column()
  created_by: string;

  @Field()
  @Column()
  updated_by: string;

  @Field(type => GraphQLISODateTime)
  @Column()
  created_at: Date;

  @Field(type => GraphQLISODateTime)
  @Column()
  updated_at: Date;

  @Field(type => [Int])
  @Column('bigint', { array: true })
  users: any;

  @Field()
  @Column()
  user_type: string;

  @Field()
  @Column()
  on_order: boolean;
}
