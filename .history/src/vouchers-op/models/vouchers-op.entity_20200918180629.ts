import {
  Field,
  GraphQLISODateTime,
  ID,
  ObjectType,
  Int,
} from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn, Index } from 'typeorm';

@ObjectType()
@Entity('vouchers_op')
export class Vouchers_op {
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  //   @Index('vouchers_op_pkey')
  id: bigint;

  @Field()
  @Column({ nullable: true })
  code: string;

  @Field()
  @Column({ nullable: true })
  type: string;

  @Field()
  @Column({ nullable: true })
  name: string;

  @Field()
  @Column({ nullable: true })
  name_ar: string;

  @Field()
  @Column({ nullable: true })
  is_multiple: boolean;

  @Field()
  @Column({ nullable: true })
  is_active: boolean;

  @Field()
  @Column({ nullable: true })
  auto_apply: boolean;

  @Field()
  @Column({ nullable: true })
  status: string;

  @Field()
  @Column({ nullable: true })
  relation_type: string;

  @Field(type => [Int])
  @Column('bigint', { array: true })
  relation: any;

  @Field()
  @Column({ nullable: true })
  voucher_type: string;

  @Field()
  @Column({ nullable: true })
  percentage: number;

  @Field()
  @Column({ nullable: true })
  min_order_amount: number;

  @Field()
  @Column({ nullable: true })
  max_order_amount: number;

  @Field()
  @Column({ nullable: true })
  amount_used_till_date: number;

  @Field()
  @Column({ nullable: true })
  min_discount_amount: number;

  @Field()
  @Column({ nullable: true })
  max_discount_amount: number;

  @Field()
  @Column({ nullable: true })
  min_quantity: number;

  @Field()
  @Column({ nullable: true })
  max_quantity: number;

  @Field()
  @Column({ nullable: true })
  max_quota: number;

  @Field()
  @Column({ nullable: true })
  country: number;

  @Field()
  @Column({ nullable: true })
  city: number;

  @Field()
  @Column({ nullable: true })
  currency_type: string;

  @Field()
  @Column({ nullable: true })
  communication_channel: string;

  @Field()
  @Column({ nullable: true })
  offer_ratio: string;

  @Field()
  @Column({ nullable: true })
  dataareaid: number;

  @Field()
  @Column({ nullable: true })
  terms: string;

  @Field()
  @Column({ nullable: true })
  terms_ar: string;

  @Field(type => GraphQLISODateTime)
  @Column({ nullable: true })
  start_date: Date;

  @Field(type => GraphQLISODateTime)
  @Column({ nullable: true })
  end_date: Date;

  @Field()
  @Column({ nullable: true })
  created_by: string;

  @Field()
  @Column({ nullable: true })
  updated_by: string;

  @Field(type => GraphQLISODateTime)
  @Column({ nullable: true })
  created_at: Date;

  @Field(type => GraphQLISODateTime)
  @Column({ nullable: true })
  updated_at: Date;

  @Field(type => [Int])
  @Column('bigint', { array: true, nullable: true })
  users: any;

  @Field()
  @Column({ nullable: true })
  user_type: string;

  @Field()
  @Column({ nullable: true })
  on_order: boolean;
}
