import { Optional } from '@nestjs/common';
import {
  Field,
  GraphQLISODateTime,
  ID,
  ObjectType,
  Int,
  InputType,
} from '@nestjs/graphql';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  Index,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@InputType('vouchers_opInput')
@ObjectType('vouchers_op')
@Entity('vouchers_op')
export class Vouchers_op {
  @Optional()
  @Field(() => ID, { nullable: true })
  @PrimaryGeneratedColumn()
  //   @Index('vouchers_op_pkey')
  id: bigint;

  @Field({ nullable: true })
  @Column({ nullable: true })
  code: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  type: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  name: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  name_ar: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  is_multiple: boolean;

  @Field({ nullable: true })
  @Column({ nullable: true })
  is_active: boolean;

  @Field({ nullable: true })
  @Column({ nullable: true })
  auto_apply: boolean;

  @Field({ nullable: true })
  @Column({ nullable: true })
  status: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  relation_type: string;

  @Field(type => [Int], { nullable: true })
  @Column('bigint', { array: true, nullable: true, default: {} })
  relation: any;

  @Field({ nullable: true })
  @Column({ nullable: true })
  voucher_type: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  percentage: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  min_order_amount: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  max_order_amount: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  amount_used_till_date: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  min_discount_amount: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  max_discount_amount: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  min_quantity: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  max_quantity: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  max_quota: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  country: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  city: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  currency_type: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  communication_channel: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  offer_ratio: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  dataareaid: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  terms: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  terms_ar: string;

  @Field(type => GraphQLISODateTime, { nullable: true })
  @Column({ nullable: true })
  start_date: Date;

  @Field(type => GraphQLISODateTime, { nullable: true })
  @Column({ nullable: true })
  end_date: Date;

  @Field({ nullable: true })
  @Column({ nullable: true })
  created_by: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  updated_by: string;

  @Field(type => GraphQLISODateTime, { nullable: true })
  @Column({ nullable: true })
  @CreateDateColumn()
  created_at: Date;

  @Field(type => GraphQLISODateTime, { nullable: true })
  @Column({ nullable: true })
  @UpdateDateColumn()
  updated_at: Date;

  @Field(type => [Int], { nullable: true })
  @Column('bigint', { array: true, nullable: true, default: {} })
  users: any;

  @Field({ nullable: true })
  @Column({ nullable: true })
  user_type: string;

  @Field({ nullable: true })
  @Column({ nullable: true, default: false })
  on_order: boolean;
}
