import { Module } from '@nestjs/common';
import { VouchersOpService } from './vouchers-op.service';
import { VouchersOpResolver } from './vouchers-op.resolver';

@Module({
  providers: [VouchersOpService, VouchersOpResolver]
})
export class VouchersOpModule {}
