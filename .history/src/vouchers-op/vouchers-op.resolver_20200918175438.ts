import { Resolver } from '@nestjs/graphql';
import { Vouchers_op } from './models/vouchers-op.entity';

@Resolver(type => Vouchers_op)
export class VouchersOpResolver {}
