import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { Vouchers_op } from './models/vouchers-op.entity';
import { VouchersOpService } from './vouchers-op.service';

@Resolver(type => Vouchers_op)
export class VouchersOpResolver {
  constructor(private readonly vouchers_opService: VouchersOpService) {}

  @Query(type => [Vouchers_op])
  public async getAllvouchers_op(): Promise<Vouchers_op[]> {
    return await this.vouchers_opService.getAllVouchersOp();
  }

  @Query(type => Vouchers_op)
  public async getvouchers_op(@Args('id') id: number): Promise<Vouchers_op> {
    return await this.vouchers_opService.getVouchersOp(id);
  }

  @Mutation(type => Vouchers_op)
  /**
   * addVoucherOp
   */
  public addVoucherOp() {}
}
