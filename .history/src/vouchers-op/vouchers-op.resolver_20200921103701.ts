import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { Vouchers_opInput } from './inputs/voucher-op.input';
import { Vouchers_op } from './models/vouchers-op.entity';
import { VouchersOpService } from './vouchers-op.service';

@Resolver(type => Vouchers_op)
export class VouchersOpResolver {
  constructor(private readonly vouchers_opService: VouchersOpService) {}

  @Query(type => [Vouchers_op])
  public async getAllvouchers_op(): Promise<Vouchers_op[]> {
    return await this.vouchers_opService.getAllVouchersOp();
  }

  @Query(type => Vouchers_op)
  public async getvouchers_op(@Args('id') id: number): Promise<Vouchers_op> {
    return await this.vouchers_opService.getVouchersOp(id);
  }

  /**
   * addVoucherOp
   */
  @Mutation(type => Vouchers_op)
  public addVoucherOp(
    @Args('vouchersOp') vouchersOp: Vouchers_opInput,
  ): Promise<Vouchers_op> {}

  /**
   * updateVoucherOp
   */
  @Mutation(type => Vouchers_op)
  public updateVoucherOp(
    @Args('id') id: number,
    @Args('vouchersOp') vouchersOp: Vouchers_opInput,
  ): Promise<Vouchers_op> {}

  /**
   * deleteVoucherOp
   */
  @Mutation(type => Vouchers_op)
  public deleteVoucherOp(@Args('id') id: number): Promise<Vouchers_op> {}
}
