import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Vouchers_op } from './models/vouchers-op.entity';

@Injectable()
export class VouchersOpService {
  constructor(
    @InjectRepository(Vouchers_op)
    private readonly vouchers_opRepo: Repository<Vouchers_op>,
  ) {}
}
