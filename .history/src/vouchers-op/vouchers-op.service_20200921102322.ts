import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Vouchers_opInput } from './inputs/voucher-op.input';
import { Vouchers_op } from './models/vouchers-op.entity';

@Injectable()
export class VouchersOpService {
  constructor(
    @InjectRepository(Vouchers_op)
    private readonly vouchers_opRepo: Repository<Vouchers_op>,
  ) {}

  public async getAllVouchersOp(): Promise<Vouchers_op[]> {
    return this.vouchers_opRepo.find();
  }

  public async getVouchersOp(id): Promise<Vouchers_op> {
    return this.vouchers_opRepo.findOne({ id });
  }

  public async addVoucherOp(voucherOp: Vouchers_opInput): Promise<Vouchers_op> {
    const newVoucherOp = new Vouchers_op();
    Object.assign(newVoucherOp, voucherOp);
    return await this.vouchers_opRepo.save(newVoucherOp);
  }

  public async updateVoucherOp(
    id,
    voucherOp: Vouchers_opInput,
  ): Promise<Vouchers_op> {}
}
