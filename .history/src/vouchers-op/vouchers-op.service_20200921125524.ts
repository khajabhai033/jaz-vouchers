import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Vouchers_opInput } from './inputs/voucher-op.input';
import { Vouchers_op } from './models/vouchers-op.entity';

@Injectable()
export class VouchersOpService {
  constructor(
    @InjectRepository(Vouchers_op)
    private readonly vouchers_opRepo: Repository<Vouchers_op>,
  ) {}

  public async getAllVouchersOp(): Promise<Vouchers_op[]> {
    return await this.vouchers_opRepo.find();
  }

  public async getVouchersOp(id): Promise<Vouchers_op> {
    const voucherOp = await this.vouchers_opRepo.findOne({ id });

    if (!voucherOp)
      throw new NotFoundException(
        `Voucher Op doesn\'t exist with the given id [${id}]`,
      );

    return voucherOp;
  }

  public async addVoucherOp(voucherOp: Vouchers_opInput): Promise<Vouchers_op> {
    const newVoucherOp = new Vouchers_op();
    Object.assign(newVoucherOp, voucherOp);
    return await this.vouchers_opRepo.save(newVoucherOp);
  }

  public async updateVoucherOp(
    id,
    voucherOpUpdate: Vouchers_opInput,
  ): Promise<Vouchers_op> {
    const voucherOp = await this.getVouchersOp(id);

    Object.assign(voucherOp, voucherOpUpdate);

    return await this.vouchers_opRepo.save(voucherOp);
  }

  public async deleteVoucher(id): Promise<Vouchers_op> {
    let voucherOp = await this.getVouchersOp(id);
    voucherOp = await this.vouchers_opRepo.remove(voucherOp);
    return voucherOp;
  }
}
