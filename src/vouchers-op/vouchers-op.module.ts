import { Module } from '@nestjs/common';
import { VouchersOpService } from './vouchers-op.service';
import { VouchersOpResolver } from './vouchers-op.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Vouchers_op } from './models/vouchers-op.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Vouchers_op])],
  providers: [VouchersOpService, VouchersOpResolver],
})
export class VouchersOpModule {}
