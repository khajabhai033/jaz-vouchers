import { Test, TestingModule } from '@nestjs/testing';
import { VouchersOpResolver } from './vouchers-op.resolver';

describe('VouchersOpResolver', () => {
  let resolver: VouchersOpResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VouchersOpResolver],
    }).compile();

    resolver = module.get<VouchersOpResolver>(VouchersOpResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
