import { Test, TestingModule } from '@nestjs/testing';
import { VouchersOpService } from './vouchers-op.service';

describe('VouchersOpService', () => {
  let service: VouchersOpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VouchersOpService],
    }).compile();

    service = module.get<VouchersOpService>(VouchersOpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
